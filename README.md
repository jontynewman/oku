# JontyNewman\Oku

A simple content management framework.

## Installation

```
composer require 'jontynewman/oku ^1.0'
```

## Example Usage

### Hello, world!

In the following example, requests to the root path (i.e. `/`) will be responded
to with the plain text response `'Hello, <path>!'`, with `<path>` being the path
of the request. All other paths will be responded to with `404 Not Found`.

It is assumed that the method and path of the request will be read from
`$_SERVER['REQUEST_METHOD']` and `$_SERVER['REQUEST_URI']` respectively.

```php
<?php

use GuzzleHttp\Psr7\Response;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilderInterface;

require 'vendor/autoload.php';

$root = function (
        ResponseBuilderInterface $builder,
        ContextInterface $context
) {

    $builder->code(200);
    $builder->headers(['Content-Type' => 'text/plain']);
    $builder->content(function () use ($context)  {
        echo "Hello, '{$context->request()->getUri()->getPath()}'!";
    });
};

$repository = new ArrayObject(['/' => $root]);

$default = new Response(404, ['Content-Type' => 'text/plain'], 'Not Found');

$handler = new RequestHandler($repository, $default);

$handler->run();

```

### Editor

To enable content management, an editor must be passed to the request handler.

```php
$editor = function (
        ResponseBuilderInterface $builder,
        ContextInterface $context
) {

    $builder->content(function () use ($context) {

        require 'editor.php';
    });
};

$handler = new RequestHandler($repository, $default, $editor);

```

An example template of the editor (i.e. `editor.php`) follows.

```php
<?php /* @var $context \JontyNewman\Oku\ContextInterface */ ?>
<!DOCTYPE html>
<html>
  <head>
    <title>JontyNewman\Oku</title>
  </head>
  <body>
    <?php /* Allow the content to be updated. */ ?>
    <form action="" method="post">
      <p>
        <label>
          Text
          <textarea name="text"></textarea>
        </label>
      </p>
      <p>
        <?php /* Prevent cross-site request forgery. */ ?>
        <?= $context->token(); ?>
        <?php /* Override POST with PUT. */ ?>
        <?= $context->put(); ?>
        <input type="submit" value="Update">
      </p>
    </form>
    <?php /* Allow the content to be deleted. */ ?>
    <form action="" method="post">
      <p>
        <?php /* Prevent cross-site request forgery. */ ?>
        <?= $context->token(); ?>
        <?php /* Override POST with DELETE. */ ?>
        <?= $context->delete(); ?>
        <input type="submit" value="Delete">
      </p>
    </form>
    <?php /* Render the current content in an iframe. */ ?>
    <?= $context->inset(); ?>
  </body>
</html>

```

### Repository

Repositories are objects that implement the `\ArrayAccess` interface.

`GET` requests will retrieve the value at the offset that is equivalent to the
path of the request. The expected return value is a `callable` that will accept
a `\JontyNewman\Oku\ResponseBuilderInterface` as the first argument and a
`\JontyNewman\Oku\ContextInterface` as the second.

`PUT` requests will set the value as a PSR-compliant request at the offset that
is equivalent to the path of the request.

`DELETE` requests will unset the offset that is equivalent to the path of the
request.

In summary, PSR-compliant requests go into the repository, and `callable` values
come out of the repository.

> Chickens go in, pies come out.
>
> *- Mrs Tweedy, 2000*

The method for persisting the repository should be determined by the developer.

## Configuration

The recommended configuration is to rewrite all requests to a single PHP file
(most commonly named `index.php`), with the request path being passed via the
server API to `$_SERVER['REQUEST_URI']`.

For more information, read
[ShrooPHP\Framework Configuration](https://gitlab.com/shroophp/framework#configuration).
