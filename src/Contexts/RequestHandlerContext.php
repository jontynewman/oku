<?php

namespace JontyNewman\Oku\Contexts;

use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\ContextTrait;
use JontyNewman\Oku\Context\InsetInterface;
use JontyNewman\Oku\Context\Insets\IframeInset;
use JontyNewman\Oku\Context\InputInterface;
use JontyNewman\Oku\Context\Inputs\HiddenInput;
use JontyNewman\Oku\Context\SessionInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * A context associated with given content retrieved via the request handler.
 */
class RequestHandlerContext implements ContextInterface
{
	use ContextTrait;

	/**
	 * The current request.
	 *
	 * @var \Psr\Http\Message\ServerRequestInterface
	 */
	private $request;

	/**
	 * The current session.
	 *
	 * @var \JontyNewman\Oku\Context\SessionInterface
	 */
	private $session;

	/**
	 * The current content.
	 *
	 * @var \JontyNewman\Oku\Context\InsetInterface
	 */
	private $inset;

	/**
	 * The key being associated with method overrides.
	 *
	 * @var string
	 */
	private $method;

	/**
	 * The cross-site request forgery prevention token.
	 *
	 * @var \JontyNewman\Oku\Context\InputInterface
	 */
	private $token;

	/**
	 * Constructs a context associated with given content retrieved via the
	 * request handler.
	 *
	 * @param ServerRequestInterface $request The current request.
	 * @param SessionInterface $session The current session.
	 * @param string $inset The current content.
	 * @param string $method The key to associate with method overrides.
	 * @param string $token The key to associate with cross-site request forgery
	 * prevent tokens.
	 * @param string $expected The value of the cross-site request forgery
	 * token.
	 */
	public function __construct(
			ServerRequestInterface $request,
			SessionInterface $session,
			string $inset,
			string $method,
			string $token,
			string $expected
	) {
		$this->request = $request;
		$this->session = $session;
		$this->inset = new IframeInset($inset);
		$this->method = $method;
		$this->token = new HiddenInput($token, $expected);
	}

	public function request(): ServerRequestInterface
	{
		return $this->request;
	}

	public function session(): SessionInterface
	{
		return $this->session;
	}

	public function method(string $method): InputInterface
	{
		return new HiddenInput($this->method, $method);
	}

	public function token(): InputInterface
	{
		return $this->token;
	}

	public function inset(): InsetInterface
	{
		return $this->inset;
	}
}
