<?php

namespace JontyNewman\Oku;

use ArrayAccess;
use GuzzleHttp\Psr7\ServerRequest;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use JontyNewman\Oku\ResponseBuilderInterface;
use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ShrooPHP\Core\Request;
use ShrooPHP\Core\Request\Response;
use ShrooPHP\Core\Session;
use ShrooPHP\Framework\Application;
use ShrooPHP\PSR\RequestHandler as PsrRequestHandler;
use function ShrooPHP\PSR\to_server_request;
use UnexpectedValueException;

/**
 * A request handler representing an instance of the application.
 */
class RequestHandler implements RequestHandlerInterface
{
	/**
	 * The default flag used to bypass the editor.
	 */
	const INSET = '_inset';

	/**
	 * The repository containing all content.
	 *
	 * @var \ArrayAccess
	 */
	private $repository;

	/**
	 * The default response.
	 *
	 * @var \Psr\Http\Message\ResponseInterface
	 */
	private $default;

	/**
	 * The editor being used to edit content (if any).
	 *
	 * @var callable|null
	 */
	private $editor;

	/**
	 * The target origin to pass to the underlying request handler (if any).
	 *
	 * @var string|null
	 */
	private $origin;

	/**
	 * The session to pass to the underlying request handler (if any).
	 *
	 * @var \ShrooPHP\Core\Session|null
	 */
	private $session;

	/**
	 * The method key to pass to the underlying request handler (if any).
	 *
	 * @var string|null
	 */
	private $method;

	/**
	 * The token key to pass to the underlying request handler (if any).
	 *
	 * @var string|null
	 */
	private $token;

	/**
	 * The key to use in order to bypass the editor (or NULL to use the
	 * default).
	 *
	 * @var string|null
	 */
	private $inset;

	/**
	 * The underlying request handler (or NULL if it is yet to be instantiated).
	 *
	 * @var \ShrooPHP\PSR\RequestHandler|null
	 */
	private $handler;

	/**
	 * Constructs a request handler.
	 *
	 * @param \ArrayAccess $repository The content to associate with the
	 * application.
	 * @param \Psr\Http\Message\ResponseInterface $default The default response.
	 * @param callable|null $editor The editor to associate with content (or
	 * NULL if all content should be immutable with no editor displayed).
	 * @param string|null $origin The origin to validate as the source origin
	 * for all mutable requests (or NULL if no validation should be performed).
	 * @param \ShrooPHP\Core\Session|null $session The session to associate with
	 * incoming requests (or NULL to use the default).
	 * @param string|null $method The key to associate with method overrides (or
	 * NULL to use the default).
	 * @param string|null $token The key to associate with cross-site request
	 * forgery prevention tokens (or NULL to use the default).
	 * @param string|null $inset The key to associate with bypassing the editor
	 * (or NULL to use the default.
	 */
	public function __construct(
			ArrayAccess $repository,
			ResponseInterface $default,
			callable $editor = null,
			string $origin = null,
			Session $session = null,
			string $method = null,
			string $token = null,
			string $inset = null
	) {
		$this->repository = $repository;
		$this->default = $default;
		$this->editor = $editor;
		$this->origin = $origin;
		$this->session = $session;
		$this->method = $method;
		$this->token = $token;
		$this->inset = $inset ?? self::INSET;
	}

	public function handle(ServerRequestInterface $request): ResponseInterface
	{
		return $this->handler()->handle($request);
	}

	/**
	 * Responds to the given request.
	 *
	 * @param \ShrooPHP\Core\Request $request the request to respond to
	 * @return \ShrooPHP\Core\Request\Response|null the response to the request
	 * (if any)
	 */
	public function __invoke(Request $request): ?Response
	{
		return $this->handler()->__invoke($request);
	}

	/**
	 * Extracts the current request from the server API and presents the
	 * response accordingly.
	 */
	public function run(): void
	{
		$this->handler()->serve();
	}

	/**
	 * Gets (or generates if it is yet to be instantiated) the underlying
	 * request handler.
	 *
	 * @return \ShrooPHP\PSR\RequestHandler The underlying request handler.
	 */
	private function handler(): PsrRequestHandler
	{
		if (is_null($this->handler)) {
			$this->handler = $this->toHandler();
		}

		return $this->handler;
	}

	/**
	 * Converts the current state of the request handler to an underlying
	 * request handler.
	 *
	 * @return \ShrooPHP\PSR\RequestHandler The converted request handler.
	 */
	private function toHandler(): PsrRequestHandler
	{
		$handler = new PsrRequestHandler(
			$this->default,
			null,
			null,
			$this->session
		);
		$handler->apply($this->toModifier());
		return $handler;
	}

	/**
	 * Converts the current state of the request handler to an application
	 * modifier.
	 *
	 * @return callable The converted request handler.
	 */
	private function toModifier(): callable
	{
		$modifier = function (Application $app): void {

			$app->method('GET', $app->_($this->get()));
		};

		if (!is_null($this->editor)) {

			$modifier = $this->tokenize($modifier);
		}

		return $modifier;
	}

	/**
	 * Generates a context using the given request, session and token.
	 *
	 * @param \Psr\Http\Message\ServerRequestInterface $request The request to
	 * associate with the context.
	 * @param \ShrooPHP\Core\Session $session The session to associate with the
	 * context.
	 * @param string $expected The token to associate with the context.
	 * @return \JontyNewman\Oku\ContextInterface The generated context.
	 */
	private function toContext(
			ServerRequestInterface $request,
			Session $session,
			string $expected = null
	): ContextInterface {

		return new RequestHandlerContext(
				$request,
				new RequestHandlerSession($session),
				$this->inset,
				$this->method ?? Application::METHOD,
				$this->token ?? Application::TOKEN,
				$expected
		);
	}

	/**
	 * Converts the given request to a server request.
	 *
	 * @param \ShrooPHP\Core\Request $request The request to convert.
	 * @return \Psr\Http\Message\ServerRequestInterface The converted request.
	 */
	private function toServerRequest(Request $request): ServerRequestInterface
	{
		$converted = $this->handler()->toServerRequest($request->root());

		if (is_null($converted)) {
			$converted = to_server_request($request);
		}

		return $converted;
	}

	/**
	 * Extracts a response (if any) from the repository associated with the
	 * request handler.
	 *
	 * @param string $id The ID of the content to retrieve.
	 * @param \JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder
	 * $builder The builder to use in order to build the response.
	 * @param \JontyNewman\Oku\ContextInterface $context The context to
	 * associate with the content.
	 * @return \ShrooPHP\Core\Request\Response|null The response (if any).
	 * @throws UnexpectedValueException The content is not callable.
	 */
	private function toResponse(
			string $id,
			RequestHandlerResponseBuilder $builder,
			ContextInterface $context
	): ?Response {

		$response = null;
		$callback = null;

		if ($this->repository->offsetExists($id)) {

			$callback = $this->repository->offsetGet($id);

			if (!is_callable($callback)) {
				$exported = var_export($callback, true);
				$message = "Expected callable but got {$exported}";
				throw new UnexpectedValueException($message);
			}
		}

		if (!is_null($callback)) {

			($callback)($builder, $context);
			$response = $builder->response();
		}

		return $response;
	}

	/**
	 * Converts to given location to a redirect response.
	 *
	 * @param string $location The location to convert.
	 * @return \ShrooPHP\Framework\Request\Responses\Response The converted
	 * location.
	 */
	private function toRedirect(string $location): Response
	{
		$builder = new RequestHandlerResponseBuilder();

		$builder->code(303); // See Other
		$builder->headers(['Location' => $location]);

		return $builder->response();
	}

	/**
	 * Adds cross-site request forgery prevention to the given application
	 * modifier.
	 *
	 * @param callable $immutable The application modifier to add cross-site
	 * request forgery prevention to.
	 * @return callable The application modifier with cross-site request forgery
	 * prevention added.
	 */
	private function tokenize(callable $immutable): callable
	{
		$mutable = function (Application $app): void {

			$app->valid(function (Application $app): void {

				$app->web($this->method, $this->toMutable());
			});
		};

		if (!is_null($this->origin)) {
			$mutable = $this->originate($this->origin, $mutable);
		}

		$modifier = function (Application $app) use ($immutable, $mutable) {

			$immutable($app);
			$mutable($app);
		};

		return function (Application $app) use ($modifier) {

			$app->token($this->token, $modifier);
		};
	}

	/**
	 * Generates an application modifier that allows changes to content within
	 * the associated repository.
	 *
	 * @return callable The generated application modifier.
	 */
	private function toMutable(): callable
	{
		return function (Application $app) {

			$methods = [
				'put' => $this->put(),
				'delete' => $this->delete(),
			];

			foreach ($methods as $method => $callback) {

				$app->method(strtoupper($method), $app->_($callback));
			}
		};
	}

	/**
	 * Adds validation of the source origin to the given application modifier.
	 *
	 * @param string $origin The target origin.
	 * @param callable $mutable The application modifier to add source origin
	 * validation to.
	 * @return callable The application modifier with source origin validation
	 * added.
	 */
	private function originate(string $origin, callable $mutable): callable
	{
		return function (Application $app) use ($origin, $mutable): void {

			$app->origin($origin, $mutable);
		};
	}

	/**
	 * Generates a request handler for retrieving content.
	 *
	 * @return callable A request handler for retrieving content.
	 */
	private function get(): callable
	{
		return function (Request $request): ?Response {

		$response = null;
		$builder = new RequestHandlerResponseBuilder();
		$psr = $this->toServerRequest($request);
		$expected = $request->token()->expected();
		$context = $this->toContext($psr, $request->session(), $expected);

		if (is_null($this->editor) || $request->params()->get($this->inset)) {

			$response = $this->toResponse($request->path(), $builder, $context);

		} else {

			($this->editor)($builder, $context);
			$response = $builder->response();
		}

		return $response;

		};
	}

	/**
	 * Generates a request handler for updating content.
	 *
	 * @return callable A request handler for updating content.
	 */
	private function put(): callable
	{
		return function (Request $request): Response {

			$psr = $this->toServerRequest($request);

			$this->repository->offsetSet($request->path(), $psr);

			return $this->toRedirect($psr->getUri()->getPath());
		};
	}

	/**
	 * Generates a request handler for deleting content.
	 *
	 * @return callable A request handler for deleting content.
	 */
	private function delete(): callable
	{
		return function (Request $request): ?Response {

			$response = null;
			$id = $request->path();

			if ($this->repository->offsetExists($id)) {
				$this->repository->offsetUnset($id);
				$response = $this->toRedirect($id);
			}

			return $response;
		};
	}
}
