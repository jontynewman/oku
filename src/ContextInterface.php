<?php

namespace JontyNewman\Oku;

use JontyNewman\Oku\Context\InsetInterface;
use JontyNewman\Oku\Context\InputInterface;
use JontyNewman\Oku\Context\SessionInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * A context associated with given content.
 */
interface ContextInterface
{
	/**
	 * Gets the current request.
	 *
	 * @return \Psr\Http\Message\ServerRequestInterface The current request.
	 */
	public function request(): ServerRequestInterface;

	/**
	 * Gets the current session.
	 *
	 * @return \JontyNewman\Oku\Context\SessionInterface The current session.
	 */
	public function session(): SessionInterface;

	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * PUT.
	 *
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function put(): InputInterface;

	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * DELETE.
	 *
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function delete(): InputInterface;

	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * the given method.
	 *
	 * @param string $method The method to override POST with.
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function method(string $method): InputInterface;

	/**
	 * Generates a HTML input that will prevent cross-site request forgery.
	 *
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function token(): InputInterface;

	/**
	 * Generates a HTML element that will embed the current content within a
	 * document.
	 *
	 * @return \JontyNewman\Oku\Context\InsetInterface The generated element.
	 */
	public function inset(): InsetInterface;
}
