<?php

namespace JontyNewman\Oku;

use JontyNewman\Oku\Context\InputInterface;

/**
 * Functionality for contexts.
 */
trait ContextTrait
{
	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * PUT.
	 *
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function put(): InputInterface
	{
		return $this->method('PUT');
	}

	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * DELETE.
	 *
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	public function delete(): InputInterface
	{
		return $this->method('DELETE');
	}

	/**
	 * Generates a HTML input that will override the POST method of a form with
	 * the given method.
	 *
	 * @param string $method The method to override POST with.
	 * @return \JontyNewman\Oku\Context\InputInterface The generated input.
	 */
	protected abstract function method(string $method): InputInterface;
}
