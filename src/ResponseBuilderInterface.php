<?php

namespace JontyNewman\Oku;

use Psr\Http\Message\ServerRequestInterface;

/**
 * A builder of responses.
 */
interface ResponseBuilderInterface
{
	/**
	 * Sets the code of the response.
	 *
	 * @param int|null $code The code of the response (or NULL to use the
	 * default).
	 */
	public function code(?int $code): void;

	/**
	 * Adds the specified header to the response.
	 *
	 * @param string $header The name of the header.
	 * @param string $value The value of the header.
	 */
	public function header(string $header, string $value): void;

	/**
	 * Resets all headers to those given (if any).
	 *
	 * @param iterable|null $headers Key-value pairs of header name to header
	 * value (with NULL being equivalent to an empty iterable).
	 */
	public function headers(?iterable $headers): void;

	/**
	 * Sets the content of the response.
	 *
	 * @param callable|null $content The content of the response (with NULL
	 * implying no content).
	 */
	public function content(?callable $content): void;
}
