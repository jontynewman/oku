<?php

namespace JontyNewman\Oku\Helpers;

/**
 * A collection of helper methods for generating HTML.
 */
class Html
{
	/**
	 * Generates a tag.
	 *
	 * @param string $name The name of the tag.
	 * @param array $attributes The attributes associated with the element.
	 * @param int|null $flags The flags to pass to the encoder (or NULL to use
	 * the default).
	 * @param string|null $encoding The encoding to pass to the encoder (or NULL
	 * to use the default.
	 * @return string The generated HTML.
	 */
	public static function tag(
			string $name,
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string {

		$tag = '<';
		$tag .= self::escape($name, $flags, $encoding);

		foreach ($attributes as $name => $value) {
			$tag .= ' ';
			$tag .= self::escape($name, $flags, $encoding);
			$tag .= '="';
			$tag .= self::escape($value, $flags, $encoding);
			$tag .= '"';
		}

		$tag .= '>';

		return $tag;
	}

	/**
	 * Escapes the given string as HTML.
	 *
	 * @param string $string The string to escape.
	 * @param int|null $flags The flags to pass to the encoder (or NULL to use
	 * the default).
	 * @param string|null $encoding The encoding to pass to the encoder (or NULL
	 * to use the default.
	 * @return string The escaped string.
	 */
	public static function escape(
			string $string,
			int $flags = null,
			string $encoding = null
	): string {

		if (is_null($flags)) {
			$flags = ENT_QUOTES | ENT_HTML5;
		}

		return is_null($encoding)
				? htmlentities($string, $flags)
				: htmlentities($string, $flags, $encoding);
	}
}
