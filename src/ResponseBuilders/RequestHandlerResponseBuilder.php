<?php

namespace JontyNewman\Oku\ResponseBuilders;

use JontyNewman\Oku\ResponseBuilderInterface;
use Psr\Http\Message\ServerRequestInterface;
use ShrooPHP\Core\Runnables\CallbackAdapter;
use ShrooPHP\Framework\Request\Responses\Response;

/**
 * A builder of responses for request handlers.
 */
class RequestHandlerResponseBuilder implements ResponseBuilderInterface
{
	/**
	 * The response being built (or NULL if it is yet to be instantiated).
	 *
	 * @var \ShrooPHP\Framework\Request\Responses\Response|null
	 */
	private $response;

	public function code(?int $code): void
	{
		$this->response()->setCode($code);
	}

	public function header(string $header, string $value): void
	{
		$this->response()->addHeader($header, $value);
	}

	public function headers(?iterable $headers): void
	{
		$this->response()->resetHeaders($headers);
	}

	public function content(?callable $content): void
	{
		if (!is_null($content)) {
			$content = new CallbackAdapter($content);
		}

		$this->response()->setContent($content);
	}

	/**
	 * Gets the response.
	 *
	 * @return \ShrooPHP\Framework\Request\Responses\Response The response.
	 */
	public function response(): Response
	{
		if (is_null($this->response)) {
			$this->response = new Response();
		}

		return $this->response;
	}
}
