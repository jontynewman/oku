<?php

namespace JontyNewman\Oku\Context\Insets;

use JontyNewman\Oku\Context\InsetInterface;
use JontyNewman\Oku\Helpers\Html;

/**
 * A piece of content that can be embedded as an iframe.
 */
class IframeInset implements InsetInterface
{
	/**
	 * The flag being used to bypass the editor.
	 *
	 * @var string
	 */
	private $flag;

	/**
	 * Constructs a piece of content that can be embedded as an iframe.
	 *
	 * @param string $flag The flag being used to bypass the editor.
	 */
	public function __construct(string $flag)
	{
		$this->flag = $flag;
	}

	public function flag(): string
	{
		return $this->flag;
	}

	public function html(
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string {

		$query = http_build_query([$this->flag() => true]);

		return Html::tag(
				'iframe',
				['src' => "?{$query}"] + $attributes,
				$flags,
				$encoding
		) . '</iframe>';
	}

	public function __toString(): string
	{
		return $this->html();
	}
}
