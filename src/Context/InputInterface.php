<?php

namespace JontyNewman\Oku\Context;

/**
 * A form input.
 */
interface InputInterface
{
	/**
	 * Gets the name of the input (without HTML being escaped).
	 *
	 * @return string The name of the input.
	 */
	public function name(): string;

	/**
	 * Gets the value of the input (without HTML being escaped).
	 *
	 * @return string The value of the input.
	 */
	public function value(): string;

	/**
	 * Converts the input to HTML.
	 *
	 * @param array $attributes The attributes to associate with the element.
	 * @param int|null $flags The flags to pass to the encoder (or NULL to use
	 * the default).
	 * @param string|null $encoding The encoding to pass to the encoder (or NULL
	 * to use the default.
	 * @return string The converted input.
	 */
	public function html(
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string;

	/**
	 * Converts the input to default HTML.
	 *
	 * @return string The converted input.
	 */
	public function __toString(): string;
}
