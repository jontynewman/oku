<?php

namespace JontyNewman\Oku\Context;

/**
 * A piece of content in an embeddable format.
 */
interface InsetInterface
{
	/**
	 * Gets the flag being used to bypass the editor (without HTML being escaped).
	 *
	 * @return string The flag being used to bypass the editor.
	 */
	public function flag(): string;

	/**
	 * Converts the embeddable content to HTML.
	 *
	 * @param array $attributes The attributes to associate with the element.
	 * @param int|null $flags The flags to pass to the encoder (or NULL to use
	 * the default).
	 * @param string|null $encoding The encoding to pass to the encoder (or NULL
	 * to use the default.
	 * @return string The converted content.
	 */
	public function html(
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string;

	/**
	 * Converts the content to default HTML.
	 *
	 * @return string The converted content.
	 */
	public function __toString(): string;
}
