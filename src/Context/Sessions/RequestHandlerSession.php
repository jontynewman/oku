<?php

namespace JontyNewman\Oku\Context\Sessions;

use JontyNewman\Oku\Context\SessionInterface;
use ShrooPHP\Core\Session;

/**
 * A repository of data associated with a session from a request handler.
 */
class RequestHandlerSession implements SessionInterface
{
	/**
	 * The underlying session.
	 *
	 * @var \ShrooPHP\Core\Session
	 */
	private $session;

	/**
	 * Constructs a repository of data associated with a session from a request
	 * handler.
	 *
	 * @param \ShrooPHP\Core\Session $session The underlying session.
	 */
	public function __construct(Session $session)
	{
		$this->session = $session;
	}

	public function offsetExists($offset): bool
	{
		return !is_null($this->session->get($offset));
	}

	public function offsetGet($offset)
	{
		return $this->session->get($offset);
	}

	public function offsetSet($offset, $value): void
	{
		$this->session->set($offset, $value);
	}

	public function offsetUnset($offset): void
	{
		$this->session->set($offset, null);
	}

	public function commit(): void
	{
		$this->session->commit();
	}
}
