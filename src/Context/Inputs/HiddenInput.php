<?php

namespace JontyNewman\Oku\Context\Inputs;

use JontyNewman\Oku\Context\InputInterface;
use JontyNewman\Oku\Helpers\Html;

/**
 * A hidden form input.
 */
class HiddenInput implements InputInterface
{
	/**
	 * The name of the input.
	 *
	 * @var string
	 */
	private $name;

	/**
	 * The value of the input.
	 *
	 * @var string
	 */
	private $value;

	/**
	 * Constructs a hidden form input.
	 *
	 * @param string $name The name of the input.
	 * @param string $value The value of the input.
	 */
	public function __construct(string $name, string $value)
	{
		$this->name = $name;
		$this->value = $value;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function value(): string
	{
		return $this->value;
	}

	public function html(
			array $attributes = [],
			int $flags = null,
			string $encoding = null
	): string {

		$input = '<input';
		$immutable = ['name' => $this->name, 'value' => $this->value];

		foreach ($immutable + $attributes as $name => $value) {

			$input .= ' ';
			$input .= Html::escape($name, $flags, $encoding);
			$input .= '="';
			$input .= Html::escape($value, $flags, $encoding);
			$input .= '"';
		}

		$input .= '>';

		return $input;
	}

	public function __toString(): string
	{
		return $this->html(['type' => 'hidden']);
	}

}
