<?php

namespace JontyNewman\Oku\Context;

use ArrayAccess;

/**
 * A repository of data associated with a session.
 */
interface SessionInterface extends ArrayAccess
{
	/**
	 * Persists the current state of the session.
	 */
	public function commit(): void;
}
