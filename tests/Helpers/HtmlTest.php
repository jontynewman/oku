<?php

namespace JontyNewman\Oku\Tests\Helpers;

use JontyNewman\Oku\Helpers\Html;
use PHPUnit\Framework\TestCase;

class HtmlTest extends TestCase
{
	public function testTag()
	{
		$name = '<"div">';
		$attributes = [
			'<"name">' => '<"value">',
			'data-<"name">' => '<"data">',
		];

		$tag = '<&lt;&quot;div&quot;&gt;'
			. ' &lt;&quot;name&quot;&gt;="&lt;&quot;value&quot;&gt;"'
			. ' data-&lt;&quot;name&quot;&gt;="&lt;&quot;data&quot;&gt;">';
		$this->assertEquals($tag, Html::tag($name, $attributes));

		$charset = 'ISO-8859-1';
		$noquote = '<&lt;"div"&gt;'
			. ' &lt;"name"&gt;="&lt;"value"&gt;"'
			. ' data-&lt;"name"&gt;="&lt;"data"&gt;">';
		$actual = Html::tag($name, $attributes, ENT_NOQUOTES, $charset);
		$this->assertEquals(mb_convert_encoding($noquote, $charset), $actual);
	}

	public function testEscape()
	{
		$string = <<<'EOF'
<>'"
EOF;
		$this->assertEquals('&lt;&gt;&apos;&quot;', Html::escape($string));

		$charset = 'ISO-8859-1';
		$expected = mb_convert_encoding('&lt;&gt;\'"', $charset);
		$actual = Html::escape($string, ENT_NOQUOTES, $charset);
		$this->assertEquals($expected, $actual);
	}
}
