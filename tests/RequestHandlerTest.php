<?php

namespace JontyNewman\Oku\Tests;

use ArrayAccess;
use GuzzleHttp\Psr7\Response as GuzzleHttpPsr7Response;
use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Uri;
use JontyNewman\Oku\ContextInterface;
use JontyNewman\Oku\Helpers\Html;
use JontyNewman\Oku\RequestHandler;
use JontyNewman\Oku\ResponseBuilderInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Session;
use ShrooPHP\Framework\Application;
use ShrooPHP\Framework\Request\Responses\Response;
use ShrooPHP\Framework\Requests\Request;
use ShrooPHP\Framework\Tokens\SessionToken;
use org\bovigo\vfs\vfsStream;
use UnexpectedValueException;

class RequestHandlerTest extends TestCase implements Session
{
	const PATH = '/';

	const HEADER = 'X-Powered-By';

	const CONTENT_CODE = Response::HTTP_PAYMENT_REQUIRED;

	const CONTENT_HEADER_VALUE = 'Dilithium';

	const EDITOR_CODE = Response::HTTP_I_AM_A_TEAPOT;

	const EDITOR_HEADER_VALUE = 'Warp Drive';

	const PREFIX = '<!-- BEGIN EDITOR -->';

	const CONTENT = 'Hello, world!';

	const SUFFIX = '<!-- END EDITOR -->';

	const ORIGIN = 'https://example.com';

	private $session;

	private $context;

	public function setUp()
	{
		parent::setUp();
		$this->session = new ArrayObject();
		$this->context = null;
	}

	public function test()
	{
		$repo = $this->toRepository();
		$default = $this->toResponse();
		$handler = new RequestHandler($repo, $default);
		$request = new ServerRequest('GET', self::PATH);

		$this->assertPsr7Response(
			$handler->handle($request),
			self::CONTENT_CODE,
			[self::HEADER => [self::CONTENT_HEADER_VALUE]],
			self::CONTENT
		);

		$post = $request->withMethod('POST');
		$this->assertSame($default, $handler->handle($post));

		$nonexistent = $request->withUri(new Uri(Response::HTTP_NOT_FOUND));
		$this->assertSame($default, $handler->handle($nonexistent));
	}

	public function testWithEditor()
	{
		$this->assertHandlerWithEditor();
	}

	public function testCustomizedWithEditor()
	{
		$this->assertHandlerWithEditor(
			self::ORIGIN,
			'method',
			'token',
			'inset'
		);
	}

	public function testInvoke()
	{
		$handler = new RequestHandler(new ArrayObject(), $this->toResponse());
		$request = new Request('GET', self::PATH);

		$this->assertNull($handler->__invoke($request));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testRun()
	{
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = self::PATH;
		$repo = $this->toRepository();
		$handler = new RequestHandler($repo, $this->toResponse());

		ob_start();
		$handler->run();
		ob_end_clean();
	}

	public function editor(
		ResponseBuilderInterface $builder,
		ContextInterface $context
	): void {

		$this->context = $context;
		$builder->code(self::EDITOR_CODE);
		$builder->header(self::HEADER, self::EDITOR_HEADER_VALUE);
		$builder->content(function () use ($context) {

			echo self::PREFIX;
			echo $context->inset();
			echo self::SUFFIX;
		});
	}

	public function content(
		ResponseBuilderInterface $builder,
		ContextInterface $context
	): void {
		$builder->code(self::CONTENT_CODE);
		$builder->header(self::HEADER, self::CONTENT_HEADER_VALUE);
		$builder->content(function () {

			echo self::CONTENT;
		});
	}

	private function toRepository(): ArrayAccess
	{
		return new ArrayObject([self::PATH => [$this, 'content']]);
	}

	private function toResponse(): ResponseInterface
	{
		return new GuzzleHttpPsr7Response(Response::HTTP_NOT_FOUND);
	}

	private function toEditor(): callable
	{
		return [$this, 'editor'];
	}

	private function assertHandlerWithEditor(
			string $origin = null,
			string $method = null,
			string $token = null,
			string $inset = null
	) {

		$exception = null;
		$repo = $this->toRepository();
		$default = $this->toResponse();
		$handler = new RequestHandler(
			$repo,
			$default,
			$this->toEditor(),
			$origin,
			$this,
			$method,
			$token,
			$inset
		);

		if (is_null($inset)) {
			$inset = RequestHandler::INSET;
		}

		$content = self::PREFIX
			. Html::tag('iframe', ['src' => "?{$inset}=1"])
			. '</iframe>'
			. self::SUFFIX;
		$headers = ['Location' => [self::PATH]];

		$request = new ServerRequest('GET', self::PATH);

		if (!is_null($origin)) {
			$invalid = $request->withMethod('POST');
			$this->assertSame($default, $handler->handle($invalid));
			$request = $request->withHeader('Origin', $origin);
		}

		$this->assertPsr7Response(
			$handler->handle($request),
			self::EDITOR_CODE,
			[self::HEADER => [self::EDITOR_HEADER_VALUE]],
			$content
		);

		if (is_null($method)) {
			$method = Application::METHOD;
		}

		if (is_null($token)) {
			$token = Application::TOKEN;
		}

		$expected = $this->session->get(SessionToken::KEY);
		$this->assertNotNull($expected);

		$data = [$token => $expected];

		$post = $request->withMethod('POST');
		$put = [$method => 'PUT'];
		$delete = [$method => 'DELETE'];

		$set_invalid = $post->withParsedBody($put);
		$this->assertSame($default, $handler->handle($set_invalid));

		$set = $post->withParsedBody($put + $data);
		$this->assertPsr7Response($handler->handle($set), 303, $headers, '');

		$this->assertSame($set, $repo->offsetGet(self::PATH));

		try {
			$handler->handle($request->withQueryParams([$inset => true]));
		} catch (UnexpectedValueException $exception) {
			$exported = var_export($set, true);
			$message = "Expected callable but got {$exported}";
			$this->assertEquals($message, $exception->getMessage());
		}

		$this->assertNotNull($exception);

		$unset_invalid = $post->withParsedBody($delete);
		$this->assertSame($default, $handler->handle($unset_invalid));

		$unset = $post->withParsedBody($delete + $data);
		$this->assertPsr7Response($handler->handle($unset), 303, $headers, '');

		$this->assertFalse($repo->offsetExists(self::PATH));
	}

	private function assertPsr7Response(
		ResponseInterface $response,
		int $code,
		array $headers,
		string $content
	): void {

		$this->assertEquals($code, $response->getStatusCode());
		$this->assertEquals($headers, $response->getHeaders());
		$this->assertEquals($content, $response->getBody()->getContents());
	}

	public function commit()
	{
		// Do nothing.
	}

	public function get($key, $default = null)
	{
		return $this->session->get($key, $default);
	}

	public function set($key, $value)
	{
		$this->session->set($key, $value);
	}
}
