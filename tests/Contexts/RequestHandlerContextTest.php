<?php

namespace JontyNewman\Oku\Tests\Contexts;

use GuzzleHttp\Psr7\ServerRequest;
use JontyNewman\Oku\Context\Insets\IframeInset;
use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use JontyNewman\Oku\Contexts\RequestHandlerContext;
use JontyNewman\Oku\Helpers\Html;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\ArrayObject;
use ShrooPHP\Core\Session;

class RequestHandlerContextTest extends TestCase implements Session
{
	private $session;

	public function setUp()
	{
		$this->session = new ArrayObject();
	}

	public function test()
	{
		$request = new ServerRequest('GET', 'http://example.org/');
		$session = new RequestHandlerSession($this);
		$context = new RequestHandlerContext(
			$request,
			$session,
			$inset = '<"inset">',
			$method = '<"method">',
			$token = '<"token">',
			$expected = '<"expected">'
		);
		$inputs = [
			'PUT' => $context->put(),
			'DELETE' => $context->delete(),
		];

		$this->assertSame($request, $context->request());
		$this->assertSame($session, $context->session());

		foreach ($inputs as $value => $input) {

			$this->assertEquals($method, $input->name());
			$this->assertEquals($value, $input->value());
			$this->assertEquals("{$context->method($value)}", "{$input}");
		}

		$value = 'POST';
		$post = $context->method($value);

		$this->assertEquals($method, $post->name());
		$this->assertEquals($value, $post->value());
		$this->assertEquals($this->toHtml($method, $value), "{$post}");

		$input = $context->token();

		$this->assertEquals($token, $input->name());
		$this->assertEquals($expected, $input->value());
		$this->assertEquals($this->toHtml($token, $expected), "{$input}");

		$expected = new IframeInset($inset);
		$actual = $context->inset();

		$this->assertEquals($expected->flag(), $actual->flag());
		$this->assertEquals("{$expected}", "{$actual}");
	}

	private function toHtml(string $name, string $value)
	{
		$attributes = [
			'name' => $name,
			'value' => $value,
			'type' => 'hidden',
		];

		return Html::tag('input', $attributes);
	}

	public function get($key, $default = null)
	{
		return $this->session->get($key, $default);
	}

	public function set($key, $value)
	{
		$this->session->set($key, $value);
	}

	public function commit()
	{
		// Do nothing.
	}
}
