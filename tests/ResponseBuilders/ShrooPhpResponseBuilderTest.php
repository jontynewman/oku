<?php

namespace JontyNewman\Oku\Tests\ResponseBuilders;

use JontyNewman\Oku\ResponseBuilders\RequestHandlerResponseBuilder;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Request\Response;

class RequestHandlerResponseBuilderTest extends TestCase
{
	public function test()
	{
		$code = 418;
		$content = 'Hello, world!';
		$header = 'Set-Cookie';
		$value = 'Chocolate Chip';
		$line = [$header => "Double {$value}"];
		$headers = [$line, [$header => $value]];
		$callback = function () use ($content) {echo $content;};
		$builder = new RequestHandlerResponseBuilder();

		$this->assertResponse($builder->response(), null, null, null);

		$builder->code($code);
		$builder->headers($line);
		$builder->content($callback);

		$this->assertResponse($builder->response(), $code, [$line], $content);

		$builder->header($header, $value);

		$this->assertResponse($builder->response(), $code, $headers, $content);

		$builder->code(null);
		$builder->headers(null);
		$builder->content(null);

		$this->assertResponse($builder->response(), null, null, null);
	}

	private function assertResponse(
		Response $response,
		?int $code,
		?iterable $headers,
		?string $content
	) {

		$actual = [];

		if (is_null($code)) {
			$this->assertNull($response->code());
		} else {
			$this->assertEquals($code, $response->code());
		}

		$traversable = $response->headers();
		$this->assertNotNull($traversable);

		if (is_null($headers)) {
			$this->assertSame([], iterator_to_array($traversable));
		} else {

			foreach ($traversable as $header => $value) {
				$actual[] = [$header => $value];
			}

			$this->assertEquals($headers, $actual);
		}

		if (is_null($content)) {
			$this->assertNull($response->content());
		} else {
			$runnable = $response->content();
			$this->assertNotNull($runnable);
			ob_start();
			$runnable->run();
			$this->assertEquals($content, ob_get_clean());
		}
	}
}
