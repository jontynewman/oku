<?php

namespace JontyNewman\Oku\Tests\Context\Insets;

use JontyNewman\Oku\Context\Insets\IframeInset;
use PHPUnit\Framework\TestCase;
use JontyNewman\Oku\Helpers\Html;

class IframeInsetTest extends TestCase
{
	public function test()
	{
		$flag = '<"flag">';
		$charset = 'ISO-8859-1';
		$flags = ENT_NOQUOTES | ENT_HTML5;
		$query = http_build_query([$flag => true]);
		$attributes = ['src' => "?{$query}"];
		$extra = ['data-name' => 'value'];
		$inset = new IframeInset($flag);

		$this->assertEquals($flag, $inset->flag());

		$this->assertEquals($inset->html(), (string) $inset);

		$this->assertEquals(
			Html::tag('iframe', $attributes + $extra) . '</iframe>',
			$inset->html(['src' => 'ignore'] + $extra)
		);

		$tag = Html::tag('iframe', $attributes + $extra, $flags, $charset);
		$this->assertEquals(
			"{$tag}</iframe>",
			$inset->html($extra, $flags, $charset)
		);
	}
}
