<?php

namespace JontyNewman\Oku\Tests\Context\Inputs;

use JontyNewman\Oku\Context\Inputs\HiddenInput;
use JontyNewman\Oku\Helpers\Html;
use PHPUnit\Framework\TestCase;

class HiddenInputTest extends TestCase
{
	public function test()
	{
		$tag = 'input';
		$name = '<"name">';
		$value = '<"value">';
		$charset = 'ISO-8859-1';
		$flags = ENT_NOQUOTES | ENT_HTML5;
		$attrs = ['name' => $name, 'value' => $value];
		$text = ['type' => 'text'];
		$hidden = ['type' => 'hidden'];
		$input = new HiddenInput($name, $value);

		$this->assertEquals($name, $input->name());
		$this->assertEquals($value, $input->value());

		$this->assertEquals(Html::tag($tag, $attrs), $input->html());
		$this->assertEquals(Html::tag($tag, $attrs + $hidden), (string) $input);

		$this->assertEquals(
			Html::tag($tag, $attrs + $text),
			$input->html($text)
		);

		$this->assertEquals(
			Html::tag($tag, $attrs + $text, $flags, $charset),
			$input->html($text, $flags, $charset)
		);
	}
}
