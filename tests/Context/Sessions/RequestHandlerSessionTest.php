<?php

namespace JontyNewman\Oku\Tests\Context\Sessions;

use JontyNewman\Oku\Context\Sessions\RequestHandlerSession;
use PHPUnit\Framework\TestCase;
use ShrooPHP\Core\Session;
use ShrooPHP\Core\ArrayObject;

class RequestHandlerSessionTest extends TestCase implements Session
{
	private $committed;

	private $session;

	public function setUp()
	{
		$this->committed = false;
		$this->session = new ArrayObject();
	}

	public function test()
	{
		$key = 'key';
		$value = 'value';
		$session = new RequestHandlerSession($this);

		$this->assertFalse($session->offsetExists($key));
		$session->offsetSet($key, $value);
		$this->assertTrue($session->offsetExists($key));
		$this->assertEquals($value, $session->offsetGet($key));
		$session->offsetUnset($key);
		$this->assertFalse($session->offsetExists($key));
		$session->commit();

		$this->assertTrue($this->committed);
	}

	public function get($key, $default = null)
	{
		return $this->session->get($key, $default);
	}

	public function set($key, $value)
	{
		$this->session->set($key, $value);
	}

	public function commit()
	{
		$this->committed = true;
	}
}
